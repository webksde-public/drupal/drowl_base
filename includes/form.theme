<?php

/**
 * @file
 * Theme and preprocess functions for forms.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;

/**
 * Iterate through form fields and set placeholder text from field title.
 *
 * @todo This wont work for fields nested in a fieldset!
 * Source: https://drupal.stackexchange.com/a/232836/44756
 * Example: add_placeholders($form);
 */
function add_placeholders(&$element) {
  if (isset($element['#type']) && in_array($element['#type'], ['textfield', 'password', 'email', 'textarea'])) {
    $element['#attributes']['placeholder'] = $element['#title'];
    $element['#attributes']['aria-label'] = $element['#title'];
    $element['#attributes']['class'][] = 'field-label-to-placeholder';
    $element['#title'] = NULL;
  }
  foreach (Element::children($element) as $key) {
    add_placeholders($element[$key]);
  }
}

/**
 * Implements hook_form_alter().
 */
function drowl_base_form_alter(&$form, &$form_state, $form_id) {
  // Add further buttons to the drupal user forms (login, password reset,
  // register):
  if (!empty($form['actions'])) {
    // Add a generic "user-form" class, to all relevant user forms.
    if ($form_id === 'user_login_form' || $form_id === 'user_pass' || $form_id === 'user_register_form') {
      $form['#attributes']['class'][] = 'user-form';
      $form['#attributes']['class'][] = 'user-form--' . Html::cleanCssIdentifier($form_id);

      // Check if anonymous users are allowed to sign up.
      $allow_registration = \Drupal::config('user.settings')->get('register') !== 'admin_only';
      // Add Sign Up Link.
      if ($form_id === 'user_login_form' || $form_id === 'user_pass') {
        if ($allow_registration) {
          $signup_link = [
            '#title' => t('Create new account'),
            '#type' => 'link',
            '#url' => Url::fromRoute('user.register'),
            '#attributes' => [
              'class' => ['btn', 'btn-link', 'btn--signup', 'px-0', 'mb-0'],
            ],
          ];
          $signup_link_wrapper = [
            '#type' => 'container',
            '#attributes' => [
              'class' => ['form-actions__link-wrapper', 'form-actions__link-wrapper--signup', 'mb-0'],
            ],
            'drowl_base_signup_link' => $signup_link,
          ];
          $form['actions']['drowl_base_signup_link'] = $signup_link_wrapper;
        }
      }
      // Add Password reset link.
      if ($form_id === 'user_login_form') {
        $password_reset_link = [
          '#title' => t("Reset your password"),
          '#type' => 'link',
          '#url' => Url::fromRoute('user.pass'),
          '#attributes' => [
            'class' => ['btn', 'btn-link', 'btn--password-reset', 'px-0', 'mb-0'],
          ],
        ];
        $password_reset_link_wrapper = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['form-actions__link-wrapper', 'form-actions__link-wrapper--password-reset', 'mb-0'],
          ],
          'drowl_base_pass_reset_link' => $password_reset_link,
        ];
        $form['actions']['drowl_base_pass_reset_link'] = $password_reset_link_wrapper;
      }
      // Add "Back to login" link.
      if ($form_id === 'user_pass' || $form_id === 'user_register_form') {
        $to_login_link = [
          '#title' => t("Log in"),
          '#type' => 'link',
          '#url' => Url::fromRoute('user.login'),
          '#attributes' => [
            'class' => ['btn', 'btn-link', 'btn--login', 'px-0', 'mb-0'],
          ],
        ];
        $to_login_link_wrapper = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['form-actions__link-wrapper', 'form-actions__link-wrapper--to-login', 'mb-0'],
          ],
          'drowl_base_to_login_link' => $to_login_link,
        ];
        $form['actions']['drowl_base_to_login_link'] = $to_login_link_wrapper;
      }
    }
  }
  // Attack User Edit Library
  if ($form_id === 'user_form') {
    $form['#attached']['library'][] = 'drowl_base/user_edit';
  }
}

/**
 * Implements template_preprocess_form().
 */
function drowl_base_preprocess_form(&$variables) {
  if (!empty($variables['attributes']) && strpos($variables['attributes']['id'], 'search-api') !== FALSE) {
    // Add a reliable class to all search forms.
    $variables['attributes']['class'][] = 'search-form';
    // Add inline form classes.
    $variables['attributes']['class'][] = 'input-group';
  }

  // Workaround for issue: https://www.drupal.org/project/drupal/issues/3001432
  // Add class to form instead of block (see _preprocess_block - we removed it
  // there):
  if (strpos($variables['attributes']['id'], 'views-exposed-form') !== FALSE) {
    $variables['attributes']['class'][] = 'views-exposed-form';
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function drowl_base_theme_suggestions_form_element_alter(&$suggestions, &$variables) {
  $element = $variables['element'];
  // Add template override suggestion by form item name
  // !! Ensure you don't have ANY dashes in your suggestion, just underscores!
  // Otherwise the suggestion shows up in twig but the tpl file isnt loaded.
  // Reset first
  $suggestions = [];
  // Add a suggestion based on the element type.
  if (isset($element['#type'])) {
    $suggestions[] = $variables['theme_hook_original'] . '__' . $element['#type'];
  }
  // Add suggestions based on the form ID.
  if (isset($element['#form_id'])) {
    $suggestions[] = 'form_element__form_id__' . $element['#form_id'];

    if (isset($element['#type'])) {
      $suggestions[] = 'form_element__form_id__' . $element['#form_id'] . '__type__' . $element['#type'];
    }
  }

  // Add suggestions for elements within Views.
  if (isset($element['#context']['#view_id'])) {
    $view_id = $element['#context']['#view_id'];
    $display_id = $element['#context']['#display_id'] ?? 'default';

    $suggestions[] = 'form_element__view__' . $view_id;
    $suggestions[] = 'form_element__view__' . $view_id . '__display__' . $display_id;

    if (isset($element['#type'])) {
      $suggestions[] = 'form_element__view__' . $view_id . '__display__' . $display_id . '__type__' . $element['#type'];
    }

    if (isset($element['#name'])) {
      // TODO: We probably should create our own "cleanSuggestionsString" function
      // The problem here was, that the name ends with an '_', so the suggestion fails.
      $name = preg_replace('/^[^a-zA-Z0-9]+|[^a-zA-Z0-9]+$/', '', $element['#name']);
      $name = strtolower(radix_clean_identifier($name));
      $suggestions[] = 'form_element__view__' . $view_id . '__display__' . $display_id . '__name__' . $name;
    }
  }

  // Add suggestions based on the elements parent (example: checkbox/radio groups)
  if(!empty($variables['element']['#parents'][0])) {
    $suggestions[] = $variables['theme_hook_original'] . '__parent__' . radix_clean_identifier($element['#parents'][0]);
  }

  // Add suggestions based on the element name.
  if (isset($element['#name'])) {
    // TODO: We probably should create our own "cleanSuggestionsString" function
    // The problem here was, that the name ends with an '_', so the suggestion fails.
    $name = preg_replace('/^[^a-zA-Z0-9]+|[^a-zA-Z0-9]+$/', '', $element['#name']);
    $name = strtolower(radix_clean_identifier($name));
    $suggestions[] = 'form_element__name__' . $name;

    if (isset($element['#type'])) {
      $suggestions[] = 'form_element__name__' . $name . '__type__' . $element['#type'];
    }
  }

  // Add suggestions based on the element ID.
  if (isset($element['#id'])) {
    $id = radix_clean_identifier($element['#id']);
    $suggestions[] = 'form_element__id__' . $id;

    if (isset($element['#type'])) {
      $suggestions[] = 'form_element__id__' . $id . '__type__' . $element['#type'];
    }
  }
}
