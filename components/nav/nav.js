// TODO:
// - add drupal behavoir stuff

const menuParentListItems = document.querySelectorAll('.navbar-nav--hover > .nav-item.dropdown');
const isTouch = 'ontouchstart' in window || !!navigator.msMaxTouchPoints;
// TODO probably get this from at data attribute?
const closeDelay = 400;

const handleMenuItemOpenState = (elem) => {
  // We need to fire an 'open' event on elem
  elem.dispatchEvent(new CustomEvent('show.bs.dropdown', { bubbles: true }));
  elem.classList.add('is-open');
  elem.querySelector('a').classList.add('show');
  elem.querySelector('a').setAttribute('aria-expanded', true);
  elem.querySelector('.dropdown-menu').classList.add('show');
  elem.dispatchEvent(new CustomEvent('shown.bs.dropdown', { bubbles: true }));
};

const handleMenuItemCloseState = (elem) => {
  elem.dispatchEvent(new CustomEvent('hide.bs.dropdown', { bubbles: true }));
  elem.classList.remove('is-open');
  elem.querySelector('a').classList.remove('show');
  elem.querySelector('a').setAttribute('aria-expanded', false);
  elem.querySelector('.dropdown-menu').classList.remove('show');
  elem.dispatchEvent(new CustomEvent('hidden.bs.dropdown', { bubbles: true }));
};

const closeAll = () => {
  menuParentListItems.forEach((item) => {
    // item.classList.remove('is-open');
    handleMenuItemCloseState(item);
  });
};

// TODO: Put this function into a library? drowl_base.helper or something?
function Timer(fn, t) {
  var timerObj = setInterval(fn, t);
  this.stop = function() {
      if (timerObj) {
          clearInterval(timerObj);
          timerObj = null;
      }
      return this;
  }
  // start timer using current settings (if it's not already running)
  this.start = function() {
      if (!timerObj) {
          this.stop();
          timerObj = setInterval(fn, t);
      }
      return this;
  }
  // start with new or original interval, stop current interval
  this.reset = function(newT = t) {
      t = newT;
      return this.stop().start();
  }
}

let closeTimer = new Timer(function() {
  closeAll();
}, closeDelay);

for (let i = 0; i < menuParentListItems.length; i++) {
  if (!isTouch) {
    const hoverintent_options = {
      sensitivity: 10,
      interval: 50
    }
    const menuLinks = menuParentListItems[i].querySelectorAll('.dropdown-toggle');
    // Make links clickable
    for (let i = 0; i < menuLinks.length; i++) {
      // Remove all events using clone
      // !! TODO: instead search for is_hover_nav => add this as further option and drop it to the nav-link component
      // TODO: none of this works.
      // var clone = menuLinks[i].cloneNode(true);
      // menuLinks[i].parentElement.replaceChild(clone, menuLinks[i]);
      // console.log(menuLinks[i]);
      // menuLinks[i].addEventListener('click touchstart', () => {
      //   console.log(this);
      // });
    }
    hoverintent(menuParentListItems[i], function(){
      // Handler in
      // Cancel running close timeout
      closeTimer.stop();
      // Close others
      closeAll();
      handleMenuItemOpenState(menuParentListItems[i]);
    }, function(){
      // Handler out
      closeTimer.start();
    }, hoverintent_options);
  }
  // TODO: what is this for?
  // menuParentListItems[i].querySelector('a').addEventListener('click touchstart', () => {
  //   if (!menuParentListItems[i].classList.contains('is-open')) {
  //     menuParentListItems.forEach((item) => {
  //       item.classList.remove('is-open');
  //     });
  //     handleMenuItemOpenState(menuParentListItems[i]);
  //   } else {
  //     handleMenuItemCloseState(menuParentListItems[i]);
  //   }
  // });
}
