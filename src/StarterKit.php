<?php

// @todo We should use the drowl_base namespace here: @codingStandardsIgnoreLine
namespace Drupal\radix_starterkit;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Theme\StarterKitInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * The Drowl Base starter kit.
 */
final class StarterKit implements StarterKitInterface {

  /**
   * {@inheritdoc}
   */
  public static function postProcess(string $working_dir, string $machine_name, string $theme_name): void {
    $info_file = "$working_dir/$machine_name.info.yml";
    $info = Yaml::decode(file_get_contents($info_file));
    $info['interface translation project'] = $machine_name;
    $info['interface translation server pattern'] = "themes/custom/{$machine_name}/translations/%language.po";

    file_put_contents($info_file, Yaml::encode($info));

    $fs = new Filesystem();
    $fs->rename("$working_dir/config/schema/radix_starterkit.schema.yml", "$working_dir/config/schema/$machine_name.schema.yml");
    $schema_file = "$working_dir/config/schema/$machine_name.schema.yml";
    $schema = Yaml::decode(file_get_contents($schema_file));
    $schema["$machine_name.settings"] = $schema['radix_starterkit.settings'];
    unset($schema['radix_starterkit.settings']);
    $schema["$machine_name.settings"]['label'] = "$machine_name settings";
    file_put_contents($schema_file, Yaml::encode($schema));

    // Rename functions in theme includes.
    $iterator = new \IteratorIterator(new \RegexIterator(
      new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator("$working_dir/includes/"),
        \RecursiveIteratorIterator::LEAVES_ONLY
      ),
      '/' . preg_quote('.inc') . '$/'
    ));

    foreach ($iterator as $include_file => $contents) {
      if (!file_put_contents($include_file, preg_replace("/(radix_starterkit)/", "$machine_name", file_get_contents($include_file)))) {
        echo "The include file $include_file could not be written.";
      }
    }
  }

}
