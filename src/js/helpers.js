(function ($, Drupal) {
  /**
   * General fixes for Drupal core bugs
   */
  Drupal.behaviors.drowl_base_helpers = {
    attach: function (context, settings) {
      // TODO: We should use context instead document?!
      // Set Browser scrollbar width as CSS variable (basically to calculate the right width for our .viewport-width styles)
      document.documentElement.style.setProperty(
        '--global-scrollbar-width',
        Drupal.drowl_base.functions.getScrollbarWidth() + 'px',
      );

      // Set page header heights's ass css variables
      const $metaHeader = document.querySelector('.meta-header');
      if($metaHeader){
        document.documentElement.style.setProperty(
          '--page-meta-header-height',
          $metaHeader.offsetHeight + 'px',
        );
      }
      // Set page navbar height ass css variables
      const $navbar = document.querySelector('.page__navbar');
      if($navbar){
        document.documentElement.style.setProperty(
          '--page-navbar-height',
          $navbar.offsetHeight + 'px',
        );
      }

      // Add a resize event. Usage:
      // TODO: Add this example to drowl_child
      // document.addEventListener(
      //   "drowl_base:resize",
      //   (e) => {
      //     /* … */
      //   },
      //   false,
      // );
      const resizeEvent = new Event("drowl_base:resize");
      const resizeObserver = new ResizeObserver(Drupal.drowl_base.functions.debounce(function () {
        // TODO: Ajax bugs! fires multiple times (eg. when blazy media loads images or videos)
        document.dispatchEvent(resizeEvent)
      }, 600));
      resizeObserver.observe(document.querySelector('body'));


      // Get the (largest) container max-width and add a class if the viewport is smaller
      let containerMaxWidth = getComputedStyle(document.querySelector('html')).getPropertyValue('--bs-container-max-width').replace(/[^0-9]/g, '');
      if (document.querySelector('body').offsetWidth < containerMaxWidth) {
        document.querySelector('body').classList.add('viewport-width-current-smaller-container-max');
      } else {
        document.querySelector('body').classList.remove('viewport-width-current-smaller-container-max');
      }

      // Update on resize
      document.addEventListener(
        "drowl_base:resize",
        (e) => {
          // TODO: Resized fires once on load, why?
          // TODO: Fires again on AJAX calls.
          // console.log('resized!');
          Drupal.drowl_base.functions.addBreakpointBodyClasses();
        },
        false,
      );

      // Detect if the user has scrolled up or down (basically to show or hide things
      // like the scroll-top button).
      // Also detect if the user has scrolled 'beyond the fold'
      const body = document.body;
      const viewportHeight = window.innerHeight;
      let oldScrollY = window.scrollY;
      window.addEventListener("scroll", Drupal.drowl_base.functions.debounce(function () {
        const scrollY = window.scrollY || window.pageYOffset;
        // Set scrolled-up/down class
        if (oldScrollY < scrollY) {
          document.querySelector('body').classList.remove('scrolled-up');
          document.querySelector('body').classList.add('scrolled-down');
        } else {
          document.querySelector('body').classList.remove('scrolled-down');
          document.querySelector('body').classList.add('scrolled-up');
        }
        oldScrollY = scrollY;
        // Set fold class
        if (scrollY > viewportHeight) {
          body.classList.add('scrolled-beyond-fold');
        } else {
          body.classList.remove('scrolled-beyond-fold');
        }
      }, 100));

      // Wrap custom (wysiwyg) tables in foundations responsive table wrapper
      $('.field--type-text-long table, .field--type-text-with-summary table, .paragraph--type-text table', context).each(function () {
        if (!$(this).parent('.table-responsive').length) {
          $(this).addClass('table').wrap('<div class="table-responsive"></div>');
        }
      });

      // Mobile search button (topbar)
      // Open offcanvas layer and focus search field
      // TODO: reinvent this? (see also "Offcanvas Modifications")
      // $('.mobile-search-button', context).on('click', function () {
      //   $('#' + $(this).data('toggle')).attr('data-drowl-base-focus-search', 'true');
      // });

      // Offcanvas Modifications
      // $(document).on('opened.zf.offCanvas', function (event) {
      //   // Auto focus the search field on offcanvas open
      //   if (typeof $(event.target).data('drowl-base-focus-search') !== 'undefined') {
      //     $(event.target).removeAttr('data-drowl-base-focus-search');
      //     // Foundation event fires before the animation has finished, so we still
      //     // need a timeout (default animation duration is 0.5s)).
      //     setTimeout(function () {
      //       $('.off-canvas__top .search-form input.form-search')[0].focus();
      //     }, 600);
      //   }
      // });

      // Initialize by class / by adding the needed additonal wrapper
      // TODO: Check if this is required by default
      // $('.form-select--fake-placeholder', context).selectFakePlaceholder();

      // Privacy form checkboxes => make the field description clickable (because there is no label)
      // TODO: Check if this is still the case with bootstrap forms
      // $('.form-item-privacypolicy-accept', context).each(function () {
      //   $(this)
      //     .find('.description:first')
      //     .on('click', function (e) {
      //       e.preventDefault();
      //       e.stopPropagation();
      //       $(this).parents('.form-item-privacypolicy-accept:first').find('label').trigger('click');
      //     });
      // });
    },
  };
})(jQuery, Drupal);
