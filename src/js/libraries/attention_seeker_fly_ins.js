// TODO: Test this
(function ($, Drupal) {
  Drupal.behaviors.drowl_base_attention_seeker_fly_ins = {
    attach: function (context, settings) {
      $('.attention-seeker-fly-in', context).each(function () {
        var $attentionSeekerBlock = $(this);
        var attentionSeekerId = $attentionSeekerBlock.attr('id');
        var stickyRegionId = 'attention-seeker-region-id-' + attentionSeekerId;
        var attentionSeekerBlockMarginTop = $attentionSeekerBlock.data('asfi-margin-top');
        var attentionSeekerBlockMarginBottom = $attentionSeekerBlock.data('asfi-margin-top');
        var $closeButton = $attentionSeekerBlock.find('.close-button:first');
        var $parentContainer = $('#page:first');
        var stickyRegionIdTop;
        var stickyRegionIdBtm;
        // Check if hide-cookie ist set
        if (Cookies.get('asfi-' + attentionSeekerId) === 'hidden') {
          $attentionSeekerBlock.addClass('attention-seeker-fly-in--hidden');
        } else {
          if ($attentionSeekerBlock.data('asfi-sticky-on') === 'container') {
            $parentContainer = $attentionSeekerBlock.parents('.paragraph--type-layout:first').length
              ? $attentionSeekerBlock.parents('.paragraph--type-layout:first')
              : $parentContainer;
            $parentContainer.attr('id', stickyRegionId);
            stickyRegionIdTop = stickyRegionId;
            stickyRegionIdBtm = stickyRegionId;
          } else if ($attentionSeekerBlock.data('asfi-sticky-on') === 'self') {
            // "Self"-Option (default)
            // Show attentsion seeker block from self TOP position
            // till the page ends.
            // Add seperate anchor/trigger element
            $attentionSeekerBlock.before('<div id="' + stickyRegionId + '" class="attention-seeker-container"></div>');
            stickyRegionIdTop = stickyRegionId;
            stickyRegionIdBtm = 'page';
          }
          new Foundation.Sticky($attentionSeekerBlock, {
            stickyOn: 'small',
            topAnchor: stickyRegionIdTop + ':top',
            btmAnchor: stickyRegionIdBtm + ':bottom',
            marginTop: attentionSeekerBlockMarginTop,
            marginBottom: attentionSeekerBlockMarginBottom,
            containerClass: 'attention-seeker-container',
          });
          // Expand the attention seeker on large devices by default
          // TODO: Add this breakpoint as data-attribute so themers are able to override just the twig file to change it.
          if (Foundation.MediaQuery.is('medium')) {
            $attentionSeekerBlock.addClass('open');
          } else {
            $attentionSeekerBlock.removeClass('open');
          }
          // Register close button => force hide block for 24h (by setting a cookie)
          $closeButton.on('click', function () {
            $attentionSeekerBlock.addClass('attention-seeker-fly-in--hidden');
            Cookies.set('asfi-' + attentionSeekerId, 'hidden', { expires: 1 });
          });
        }
      });
    },
  };
})(jQuery, Drupal);
